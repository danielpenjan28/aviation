package com.exam.aviation.repository;

import com.exam.aviation.entity.FlightDetails;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by
 * Daniel Kyle Penjan
 * Web Developer
 * on 09/07/2021
 */
public interface FlightDetailsRepository extends JpaRepository<FlightDetails, String> {

}
