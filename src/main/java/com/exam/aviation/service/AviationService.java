package com.exam.aviation.service;

import com.exam.aviation.entity.FlightDetails;
import com.exam.aviation.repository.FlightDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by
 * Daniel Kyle Penjan
 * Web Developer
 * on 09/07/2021
 */

@Service
public class AviationService {

    @Autowired
    private FlightDetailsRepository flightDetailsRepository;

    public List<FlightDetails> getFlightDetails() {
        return flightDetailsRepository.findAll();
    }

    public String saveFlightDetails(FlightDetails flightDetails) {
        return flightDetailsRepository.save(flightDetails).getFlightNumber();
    }
}
