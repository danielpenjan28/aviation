package com.exam.aviation.controllers;

import com.exam.aviation.entity.FlightDetails;
import com.exam.aviation.service.AviationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * Created by
 * Daniel Kyle Penjan
 * Web Developer
 * on 09/07/2021
 */

@RestController
public class AviationController {

    @Autowired
    private AviationService aviationService;

    @GetMapping("/flights")
    public ResponseEntity<?> getFlights() {
        HashMap<String, Object> response = new HashMap<>();
        try {
            List<FlightDetails> details = aviationService.getFlightDetails();

            response.put("FlightDetails", details);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/flights")
    public ResponseEntity<?> saveFlights(@RequestBody FlightDetails flightDetails) {
        HashMap<String, Object> response = new HashMap<>();
        try {
            String flightNumber = aviationService.saveFlightDetails(flightDetails);

            response.put("flightNumber", flightNumber);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
