package com.exam.aviation;

import com.exam.aviation.entity.FlightDetails;
import com.exam.aviation.repository.FlightDetailsRepository;
import com.exam.aviation.service.AviationService;
import javassist.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class AviationApplicationTests {

    @Mock
    private AviationService aviationService;

    @Autowired
    private FlightDetailsRepository flightDetailsRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void contextLoads() {
    }

    @Test
    @Sql("classpath:test-data.sql")
    public void getFlightDetailsTest() {
        Mockito.when(aviationService.getFlightDetails()).thenReturn(flightDetailsRepository.findAll());

        List<FlightDetails> details = aviationService.getFlightDetails();

        Assert.assertFalse(details.isEmpty());
    }

    @Test(expected = NotFoundException.class)
    public void getFailedFlightDetailsTest() {
        Mockito.when(aviationService.getFlightDetails()).thenThrow(NotFoundException.class);

        List<FlightDetails> details = aviationService.getFlightDetails();
    }

    @Test
    public void postFlightDetailsTest() {
        FlightDetails newFlight = newFlight();

        Mockito.when(aviationService.saveFlightDetails(newFlight)).thenReturn(flightDetailsRepository.save(newFlight).getFlightNumber());

        String flightNumber = aviationService.saveFlightDetails(newFlight);

        Assert.assertEquals("CX420", flightNumber);
    }

    @Test(expected = NoSuchFieldException.class)
    public void postFlightDetailsFailedTest() {
        FlightDetails flightDetails = newFlight();

        Mockito.when(aviationService.saveFlightDetails(flightDetails)).thenThrow(NoSuchFieldException.class);

        String flightNumber = aviationService.saveFlightDetails(flightDetails);
    }

    private FlightDetails newFlight() {

        return new FlightDetails(
                "CX420",
                "JPN",
                "IND",
                Timestamp.valueOf("2020-06-14 01:00:23.000"),
                Timestamp.valueOf("2020-06-14 12:00:24.000")
        );
    }
}
