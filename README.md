Aviation Airlines Backend

Technologies:  <br/>
IDE IntelliJ <br/>
Language - Java <br/>
JDK Version - Java 8 <br/>
Framework - Spring Boot <br/>
Version Control System - Git <br/>
Repository Location - Gitlab <br/>
Databases - H2(InMemory) <br/>

Sample Request:

GET localhost:8080/flights/

Success Response:

{
  "FlightDetails": [
    {
      "flightNumber": "QF400",
      "departurePort": "MEL",
      "arrivalPort": "SYD",
      "departureTime": "2020-06-10T01:00:23.000Z",
      "arrivalTime": "2020-06-10T02:25:23.000Z"
    },
    {
      "flightNumber": "EK200",
      "departurePort": "SYD",
      "arrivalPort": "MEL",
      "departureTime": "2020-06-10T01:00:23.000Z",
      "arrivalTime": "2020-06-10T18:25:23.000Z"
    }
  ]
}

Sample Request:

URL: POST localhost:8080/flights/
BODY: {
        "flightNumber": "CX420",
        "departurePort": "NYC",
        "arrivalPort": "PHL",
        "departureTime": "2020-06-10T01:00:23.000Z",
        "arrivalTime": "2020-06-11T01:25:23.000Z"
      }

Sample Response:

{
  "flightNumber": "CX420"
}

SWAGGER URL:

http://localhost:8080/swagger-ui.html

H2 CONSOLE URL:

http://localhost:8080/h2_console

